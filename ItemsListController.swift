//
//  ItemsListController.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/5/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import UIKit
import RateView
import CoreData

class ItemsListController: UITableViewController {

    
    var transfers = [TransferModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.tableView.rowHeight = 100
    }

    // MARK: - Table view data source

   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transfers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let imgView = cell.viewWithTag(1) as? UIImageView
        imgView?.layer.cornerRadius = 35
        imgView?.layer.masksToBounds = true
        
        let address = cell.viewWithTag(2) as? UILabel
        address?.text = transfers[indexPath.row].fromAddress
        
        print(transfers[indexPath.row].fromAddress)
        
        let rater = cell.viewWithTag(5) as? RateView
        rater?.starSize = 10
        rater?.canRate = true
        rater?.rating = 2.7
        rater?.starFillColor = UIColor.init(red: 250/255, green: 173/255, blue: 20/255, alpha: 1.0)
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "map") as? MapController
        dest?.transfer = transfers[indexPath.row]
        self.present(dest!, animated: true, completion: nil)
    }
}


