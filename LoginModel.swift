//
//  LoginModel.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/5/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

protocol ReturnTransfersDelegate {
    func confirmlogin()
}


class LoginModel: NSObject {
    
    var delegate: ReturnTransfersDelegate!
    var superController: UIViewController!
    var superView: UIView!
    
    
    func loginWith(name: String, pass: String){
        
        MBProgressHUD.hideAllHUDs(for: self.superView, animated: true)
        let parameters = [
            "un":"userName",
            "up":"userPassword",
            "name": name,
            "password": pass
        ]
        
        let url = URL(string: "http://qtech-system.com/interview/index.php/apis/login")
        
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON {response in
            
            MBProgressHUD.hideAllHUDs(for: self.superView, animated: true)
            
            if let result = response.result.value as? Dictionary<String, Any> {

                if result["status"] as? String == "success" {
                    
                    
                    
                    
                    //1
                    let app = UIApplication.shared.delegate as! AppDelegate
                    
                    let managedContext = app.persistentContainer.viewContext
                    
                    let request: NSFetchRequest<TransferModel>
                    
                    if #available(iOS 10.0, OSX 10.12, *) {
                        request = TransferModel.fetchRequest()
                    } else {
                        request = NSFetchRequest(entityName: "TransferModel")
                    }
                    
                    do {
                        let items = try managedContext.fetch(request) 
                        
                        for item in items {
                            managedContext.delete(item)
                        }
                        // Save Changes
                        try managedContext.save()
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    
                    let array = result["message"] as? Array<Dictionary<String, Any>>
                    for trans in array! {
                        
                        let transfer = NSEntityDescription.insertNewObject(forEntityName: "TransferModel", into: managedContext) as! TransferModel
                        
                        transfer.fromAddress = trans["FromAddress"] as? String
                        transfer.fromLat = Double((trans["FromLat"] as? String)!)!
                        transfer.fromLong = Double((trans["FromLong"] as? String)!)!
                        transfer.toAddress = trans["ToAddress"] as? String
                        transfer.toLat = Double((trans["ToLat"] as? String)!)!
                        transfer.toLong = Double((trans["ToLong"] as? String)!)!
                        
                        do {
                            try managedContext.save()
                        } catch {
                            fatalError("Failure to save context: \(error)")
                        }
                    }
                    
                    self.delegate.confirmlogin()
                }else{
                    let alertController = UIAlertController(title: "خطا في الايميل او كلمه المرور", message: nil, preferredStyle: .alert)
                    
                    let OKAction = UIAlertAction(title: "موافق", style: .default) { (action) in
                        alertController.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    
                    self.superController.present(alertController, animated: true)
                }
            }else{
                self.delegate.confirmlogin()
            }
        }
    }
    
}
