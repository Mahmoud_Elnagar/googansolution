//
//  ItemsGridController.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/5/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import UIKit
import RateView
import CoreData


class ItemsGridController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    
    var transfers = [TransferModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
        
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return transfers.count / 2
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
    
        // Configure the cell
        let imgView = cell.viewWithTag(1) as? UIImageView
        imgView?.layer.cornerRadius = 35
        imgView?.layer.masksToBounds = true
        
        let address = cell.viewWithTag(2) as? UILabel
        
        let rater = cell.viewWithTag(5) as? RateView
        rater?.starSize = 10
        rater?.canRate = true
        rater?.rating = 2.7
        rater?.starFillColor = UIColor.init(red: 250/255, green: 173/255, blue: 20/255, alpha: 1.0)
        
        let view = cell.viewWithTag(3)
        if indexPath.row == 0 {
            address?.text = transfers[indexPath.section * 2].fromAddress
            view?.isHidden = true
        }else{
            address?.text = transfers[(indexPath.section * 2) + 1].fromAddress
            view?.isHidden = false
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2.07, height: 150)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "map") as? MapController
        
        if indexPath.row == 0 {
            dest?.transfer = transfers[indexPath.section * 2]
        }else{
            dest?.transfer = transfers[(indexPath.section * 2) + 1]
        }
        self.present(dest!, animated: true, completion: nil)
    }
}
