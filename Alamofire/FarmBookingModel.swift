//
//  FarmBookingModel.swift
//  Makan
//
//  Created by Mahmoud Elnagar on 1/11/2016.
//  Copyright © 2016 com.Kickstartinteractive. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol FarmBookingDelegate {
    func returnBookConfirm(message: String)
}
class FarmBookingModel: NSObject {

    
    var delegate: FarmBookingDelegate!
    var superController: UIViewController!
    var superView: UIView!
    
    func bookAFarmWith(farm_id: Int, firstName: String, lastName: String, phone: String, email: String, from: String, to: String, comment: String) {
        
        MBProgressHUD.showAdded(to: superView, animated: true)
        let headers: HTTPHeaders = [
            "ApiKey": Constants.APIKEY,
            "Authorization": Constants.Auth
        ]
        
        let parameters: Dictionary = [
            "farm": "\(farm_id)",
            "first_name": firstName,
            "last_name": lastName,
            "phone": phone,
            "email": email,
            "from": from,
            "to": to,
            "comment": comment
        ]
        
        let url = URL(string: Constants.BASEURL + "farms/booking")!
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON {response in
            
            if let result = response.result.value{
                
                let json = JSON(result)
                let status = json["status"].dictionaryValue
                print(json)
                
                if status["code"] == 200 && status["status"] == 1 {
                    self.delegate.returnBookConfirm(message: HelperMethods.checkString(string: status["message"]))
                }else{
                    let alertController = UIAlertController(title: nil, message: HelperMethods.checkString(string: status["message"]), preferredStyle: .alert)
                    
                    let OKAction = UIAlertAction(title: "موافق", style: .default) { (action) in
                        alertController.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    
                    self.superController.present(alertController, animated: true)
                }
                MBProgressHUD.hideAllHUDs(for: self.superView, animated: true)
            }else{
                MBProgressHUD.hideAllHUDs(for: self.superView, animated: true)
            }
        }
    }
}
