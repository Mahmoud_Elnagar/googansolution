//
//  MapController.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/5/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MapController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var mapView: MKMapView!
    var transfer: TransferModel!
    
    var locationManager = CLLocationManager()
    var isStart = false

    
    var fromPlacemark: MKPlacemark!
    var toPlacemark: MKPlacemark!
    var currentPlacemark: MKPlacemark!
    var fromAnnotation: MapAnnotation!
    var toAnnotation: MapAnnotation!
    var currentAnnotation: MapAnnotation!
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
   
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // 1.
        mapView.delegate = self
        mapView.showsUserLocation = true
        self.checkLocationAuthorizationStatus()
        
        let locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()

        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        
        
        // 2.
        let fromLocation = CLLocationCoordinate2D(latitude: transfer.fromLat, longitude: transfer.fromLong)
        let toLocation = CLLocationCoordinate2D(latitude: transfer.toLat, longitude: transfer.toLong)
        let currentLocation = CLLocationCoordinate2D(latitude: ((transfer.toLat + transfer.fromLat) / 2), longitude: ((transfer.toLong + transfer.fromLong) / 2))
        // 3.
        fromPlacemark = MKPlacemark(coordinate: fromLocation, addressDictionary: nil)
        toPlacemark = MKPlacemark(coordinate: toLocation, addressDictionary: nil)
        currentPlacemark = MKPlacemark(coordinate: currentLocation, addressDictionary: nil)

        // 4.
        fromAnnotation = MapAnnotation(title: transfer.fromAddress!, coordinate: CLLocationCoordinate2D(latitude: transfer.fromLat, longitude: transfer.fromLong), image: "locationoff")
        toAnnotation = MapAnnotation(title: transfer.toAddress!, coordinate: CLLocationCoordinate2D(latitude: transfer.toLat, longitude: transfer.toLong), image: "locationon")
        currentAnnotation = MapAnnotation(title: "middle location", coordinate: CLLocationCoordinate2D(latitude: ((transfer.toLat + transfer.fromLat) / 2), longitude: ((transfer.toLong + transfer.fromLong) / 2)), image: "user")
        
        
        // 5.
        self.mapView.showAnnotations([toAnnotation, fromAnnotation, currentAnnotation], animated: true )
        drawLine(place1: currentPlacemark, place2: toPlacemark)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let center = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
       
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.green
        renderer.lineWidth = 3.0
        
        return renderer
    }
    
    func drawLine(place1: MKPlacemark, place2: MKPlacemark) {
        
        let fromMapItem = MKMapItem(placemark: place1)
        let toMapItem = MKMapItem(placemark: place2)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = fromMapItem
        directionRequest.destination = toMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is MapAnnotation) {
            return nil
        }
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: "test")
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: "test")
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        let ma = annotation as! MapAnnotation
        anView?.image = UIImage(named: ma.image!)
        
        return anView
    }
    
    @IBAction func changeRoot(_ sender: UIButton) {
        
        let overlays = mapView.overlays
        mapView.removeOverlays(overlays)
        
        
        if isStart {
            sender.setTitle("Start", for: .normal)
            isStart = false
            mapView.view(for: fromAnnotation)?.isHidden = true
            mapView.view(for: toAnnotation)?.isHidden = false
            drawLine(place1: currentPlacemark, place2: toPlacemark)
        }else{
            sender.setTitle("Finish", for: .normal)
            mapView.view(for: fromAnnotation)?.isHidden = false
            mapView.view(for: toAnnotation)?.isHidden = true
            drawLine(place1: currentPlacemark, place2: fromPlacemark)
            isStart = true
        }
    }
    
    
    @IBAction func dismissController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
