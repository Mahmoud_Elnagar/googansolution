//
//  TransferModel+CoreDataProperties.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/8/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import Foundation
import CoreData


extension TransferModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TransferModel> {
        return NSFetchRequest<TransferModel>(entityName: "TransferModel");
    }

    @NSManaged public var transferID: Int32
    @NSManaged public var fromLong: Double
    @NSManaged public var fromLat: Double
    @NSManaged public var fromAddress: String?
    @NSManaged public var toLong: Double
    @NSManaged public var toLat: Double
    @NSManaged public var toAddress: String?

}
