//
//  LoginViewController.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/5/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import UIKit
import RateView
import CoreData

class LoginViewController: UIViewController, UITextFieldDelegate, ReturnTransfersDelegate {

    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var btnLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtPassword.layer.borderColor = UIColor.red.cgColor
        txtUserName.layer.borderColor = UIColor.red.cgColor
    }
    
    @IBAction func loginActionBtn(_ sender: UIButton) {
        
        let model = LoginModel()
        model.superController = self
        model.superView = self.view
        model.delegate = self
        model.loginWith(name: txtUserName.text!, pass: txtPassword.text!)
    }
    
    @IBAction func txtDidChange(_ sender: UITextField) {
        
        if txtPassword.text != nil && ((txtPassword.text?.trimmingCharacters(in: [" "])) != "") && txtUserName.text != nil && ((txtUserName.text?.trimmingCharacters(in: [" "])) != "") {
            btnLogin.isEnabled = true
        }else{
            btnLogin.isEnabled = false
        }
    }
    
    func confirmlogin() {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "home") as? HomePageController
        self.present(dest!, animated: true, completion: nil)
    }
}
