//
//  HomeMapListingController.swift
//  Makan
//
//  Created by Mahmoud Elnagar on 30/10/2016.
//  Copyright © 2016 com.Kickstartinteractive. All rights reserved.
//

import UIKit
import CoreData

class HomePageController: UIViewController {

    @IBOutlet var containerView: UIView!

    var transfers = [TransferModel]()
    
    var listView: ItemsListController?
    var gridView: ItemsGridController?
    var islist = true
    
    private var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(inactiveViewController: oldValue)
            updateActiveViewController()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listView = self.storyboard?.instantiateViewController(withIdentifier: "listView") as? ItemsListController
        gridView = self.storyboard?.instantiateViewController(withIdentifier: "gridView") as? ItemsGridController
        
        //1
        let app = UIApplication.shared.delegate as! AppDelegate
        let managedContext = app.persistentContainer.viewContext
        
        
        let request: NSFetchRequest<TransferModel>
        
        if #available(iOS 10.0, OSX 10.12, *) {
            request = TransferModel.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "TransferModel")
        }
        
        do {
            transfers = try managedContext.fetch(request)
            
            listView?.transfers = self.transfers
            gridView?.transfers = self.transfers
            
            activeViewController = listView
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
        
    @IBAction func moveToMapViewAction(_ sender: UIButton) {
        
        if islist {
            sender.setImage(UIImage(named: "list"), for: .normal)
            activeViewController = gridView
            islist = false
        }else{
            sender.setImage(UIImage(named: "grid"), for: .normal)
            activeViewController = listView
            islist = true
        }
    }
    
    private func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            inActiveVC.willMove(toParentViewController: nil)
            inActiveVC.view.removeFromSuperview()
            inActiveVC.removeFromParentViewController()
        }
    }
    
    private func updateActiveViewController() {
        if let activeVC = activeViewController {
            addChildViewController(activeVC)
            activeVC.view.frame = containerView.bounds
            containerView.addSubview(activeVC.view)
            
            activeVC.didMove(toParentViewController: self)
        }
    }
    
}
