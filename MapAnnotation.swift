//
//  MapAnnotation.swift
//  GooganSolutionsInterview
//
//  Created by Mahmoud El_nagar on 11/5/16.
//  Copyright © 2016 Mahmoud El_nagar. All rights reserved.
//

import UIKit
import MapKit

class MapAnnotation: NSObject, MKAnnotation {

    var title: String?
    var image: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D, image: String) {
        self.title = title
        self.image = image
        self.coordinate = coordinate
        
        
        super.init()
    }
    
}
